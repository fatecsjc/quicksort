#include <stdio.h>
#include <stdlib.h>
#include <locale.h>

int particiona(int v[], int inicio, int fim){

    int esq, dir, pivo, aux;

    esq = inicio;
    dir = fim;
    pivo = v[inicio];

    while(esq < dir){

        while(v[esq] <= pivo)
            esq++;
        while(v[dir] > pivo)
            dir--;

        if(esq < dir){
            aux = v[esq];
            v[esq] = v[dir];
            v[dir] = aux;
        }
    }

    v[inicio] = v[dir];
    v[dir] = pivo;

    return dir;
}

void quickSort(int v[], int inicio, int fim){

    int pivo;
    if(fim > inicio){

        pivo = particiona(v, inicio, fim);

        quickSort(v, inicio, pivo -1);
        quickSort(v, pivo + 1, fim);
    }
}

void imprimirVetorDesordenado(int v[], int n) {
    printf("\nVetor n�o ordenado.\n");
    for (int i = 0; i < n; i++) printf("%d\t", v[i]);
}

void imprimirVetorOrdenado(int v[], int n) {
    quickSort(v, 0, 5);
    printf("\nVetor ordenado.\n");
    for (int i = 0; i < n; i++) printf("%d\t", v[i]);
}

int main()
{
    setlocale(LC_ALL, "Portuguese");

    int vetor1[6] = {2, 1, 4, 3, 6, 5};

    imprimirVetorDesordenado(vetor1, 6);
    quickSort(vetor1, 0, 6);
    imprimirVetorDesordenado(vetor1, 6);


    return 0;
}
